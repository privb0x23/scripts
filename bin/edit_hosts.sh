#! /usr/bin/env sh
# shellcheck shell=sh
# -*- coding: utf-8 -*-
# -*- mode: shell-script -*-

# Copyright (c) 2021-2022 privb0x23
# OpenBSD Licence

# set -x

# ==================================================================

dir="/${0}"; dir="${dir%/*}"; dir="${dir:-.}"; dir="${dir#/}/"
d_script="$( CDPATH='' cd -- "${dir}" && pwd -P )"
unset dir
f_conchology="${d_script}/../lib/conchology.sh"
# shellcheck source=../lib/conchology.sh disable=SC2015
test -f "${f_conchology}" && . "${f_conchology}" \
|| {
	f_conchology="${d_script}/../.lib/conchology.sh"
	# shellcheck source=../lib/conchology.sh
	test -f "${f_conchology}" && . "${f_conchology}" || exit 1
}
trap super_onexit EXIT

# ==================================================================

# shellcheck disable=SC2034
m_version='0.1.1+02021-05-23'

# ==================================================================
# ChangeLog
#
# - 0.1.0	Initial version
#
# ==================================================================

init() {

	super_init "${@}"

	f_hosts='/etc/hosts'
	# f_hosts='hosts'

	# very simplistic - needs improvement
	re_ipv4='[0-9\.]+'
	re_ipv6='[0-9a-f:]+'

	check_cmd "${c_grep}"
	check_cmd "${c_sed}"
}

# ==================================================================

edit_file() {

	msg 4 'edit file' "${*}"

	# shellcheck disable=SC2015
	assert_nz "${1}" \
	&& assert_nz "${2}" \
	&& assert_nz "${3}" \
	|| { errmsg 'invalid parameters'; return 1; }

	ret=0
	l_edit_file_f_toedit="${1}"
	l_edit_file_h_toedit="${2}"
	l_edit_file_ip_new="${3}"
	l_edit_file_h_toedit_esc=''
	l_edit_file_new_entry=1
	l_edit_file_ip_version=''

	test -f "${l_edit_file_f_toedit}" \
	|| { ret=$?; errmsg "invalid file: ${l_edit_file_f_toedit}"; }

	# escape any regex special characters (really '.' is the important one)
	l_edit_file_h_toedit_esc="$( printout "${l_edit_file_h_toedit}" \
	| ${c_sed} -E 's/([\.\*\+\[^\$])/\\\1/g' )"
	ret=$?

	# check whether the entry exists
	test ${ret} -eq 0 \
	&& ${c_grep} -qE \
		"[[:space:]]+${l_edit_file_h_toedit_esc}($|[[:space:]]+)" \
		"${l_edit_file_f_toedit}" \
	&& l_edit_file_new_entry=0

	msg 3 "new entry: ${l_edit_file_new_entry}"

	# new entry
	if test ${ret} -eq 0 && test '0' != "${l_edit_file_new_entry}"; then

		printf -- '%s\t%s\n' "${l_edit_file_ip_new}" "${l_edit_file_h_toedit}" \
		>> "${l_edit_file_f_toedit}" \
		|| { ret=$?; errmsg 'error creating new entry'; }

	# edit existing entry
	else

		# shellcheck disable=SC2015
		test ${ret} -eq 0 \
		&& printout "${l_edit_file_ip_new}" \
		| ${c_grep} -iqE "^${re_ipv4}\$" \
		&& l_edit_file_ip_version='4' \
		|| { printout "${l_edit_file_ip_new}" \
			| ${c_grep} -iqE "^${re_ipv6}\$" \
			&& l_edit_file_ip_version='6'; }

		msg 3 "ip version: ${l_edit_file_ip_version}"

		if test ${ret} -eq 0; then

			# ipv4
			if test '4' = "${l_edit_file_ip_version}"; then

				${c_sed} -i'' -E \
					"/[[:space:]]+${l_edit_file_h_toedit_esc}($|[[:space:]]+)/ s/^${re_ipv4}([[:space:]]+)/${l_edit_file_ip_new}\1/" \
					"${l_edit_file_f_toedit}" \
				|| { ret=$?; errmsg 'error editing file'; }

			# ipv6
			elif test '6' = "${l_edit_file_ip_version}"; then

				${c_sed} -i'' -E \
					"/[[:space:]]+${l_edit_file_h_toedit_esc}($|[[:space:]]+)/ s/^${re_ipv6}([[:space:]]+)/${l_edit_file_ip_new}\1/" \
					"${l_edit_file_f_toedit}" \
				|| { ret=$?; errmsg 'error editing file'; }

			else
				ret=1
				errmsg 'error invalid ip address'
			fi
		fi

	fi

	unset l_edit_file_f_toedit
	unset l_edit_file_h_toedit
	unset l_edit_file_ip_new
	unset l_edit_file_h_toedit_esc
	unset l_edit_file_new_entry
	unset l_edit_file_ip_version

	return ${ret}
}

# ==================================================================

main() {

	msg_verbose='4'

	# init
	init "${@}"

	edit_file "${f_hosts}" "${1}" "${2}"
}

main "${@}"

# ==================================================================

# vim: tabstop=4 shiftwidth=4 autoindent filetype=sh
# eof edit_hosts.sh
