# Scripts

Various shell scripts.

## Conchology

`lib/conchology.sh`

Shell script library with common useful functionality used by other scripts.

## Blake3 Git Annex Backend

`bin/git-annex-backend-XBLAKE3`

Blake3 hash, using `b3sum`, backend for git-annex. Does not require conchology.

## Helpers

`bin/edit_hosts.sh` - Simple way to add/edit entries in hosts, though not a substitue for a "proper" solution. Requires conchology.

## Licence

[OpenBSD Licence](LICENCE).

Copyright (c) 2020-2022 privb0x23
